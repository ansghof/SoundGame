import pyaudio
import numpy as np

# Constants
CHUNK = 1024  # Number of frames per buffer
FORMAT = pyaudio.paInt16  # Audio format (bytes per sample)
CHANNELS = 1  # Number of audio channels (1 for mono, 2 for stereo)
RATE = 44100  # Sample rate (samples per second)
LOWPASS = 1000
HIGHPASS = 80
THRESHOLD = 5000

stream: pyaudio.Stream = None
audio = None


def calculate_frequency(audio_data):
    fft_data = np.fft.fft(audio_data)
    freqs = np.fft.fftfreq(len(fft_data))
    idx = np.argmax(np.abs(fft_data))
    freq = freqs[idx] * RATE
    return freq


def init():
    global stream, audio
    audio = pyaudio.PyAudio()
    stream = audio.open(format=FORMAT, channels=CHANNELS, rate=RATE, input=True, frames_per_buffer=CHUNK)


def get_frequency():
    global THRESHOLD, HIGHPASS, LOWPASS
    try:
        if stream.is_stopped():
            print("Stream stopped while getting frequency")
            return 0.0

        data = stream.read(CHUNK)
        audio_data = np.frombuffer(data, dtype=np.int16)
        frequency = 0.0
        volume = np.max(np.abs(audio_data))
        if volume > THRESHOLD:
            frequency = calculate_frequency(audio_data)
        if HIGHPASS < frequency < LOWPASS:
            # print(f"Frequency: {frequency}, Volume: {volume}")
            return frequency
        else:
            return 0.0
    except Exception as e:
        print("An error occurred:", e)

def shutdown():
    global stream, audio
    # Close the stream and terminate PyAudio
    stream.stop_stream()
    stream.close()
    audio.terminate()
    print("Stream stopped.")


def get_note(frequency):
    # Define a dictionary mapping frequencies to notes
    note_mapping = {
        "G#1": 100.0,
        "A1": 110.0,
        "A#1": 116.54,
        "B1": 123.47,
        "C2": 130.81,
        "C#2": 138.59,
        "D2": 146.83,
        "D#2": 155.56,
        "E2": 164.81,
        "F2": 174.61,
        "F#2": 185.00,
        "G2": 196.00,
        "G#2": 207.65,
        "A2": 220.00,
        "A#2": 233.08,
        "B2": 246.94,
        "C3": 261.63,
        "C#3": 277.18,
        "D3": 293.66,
        "D#3": 311.13,
        "E3": 329.63,
        "F3": 349.23,
        "F#3": 369.99,
        "G3": 392.00,
        "G#3": 415.30,
        "A3": 440.00,
        "A#3": 466.16,
        "B3": 493.88,
        "C4": 523.25,
        "C#4": 554.37,
        "D4": 587.33,
        "D#4": 622.25,
        "E4": 659.25,
        "F4": 698.46,
        "F#4": 739.99,
        "G4": 783.99,
        "G#4": 830.61,
        "A4": 880.00,
        "A#4": 932.33,
        "B4": 987.77,
        "C5": 1046.50,
    }
    # Find the closest frequency in the dictionary
    closest_note = min(note_mapping, key=lambda x: abs(note_mapping[x] - frequency))

    return closest_note