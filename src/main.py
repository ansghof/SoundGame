import threading
import time
from enum import Enum

import pygame
import sys
import frequency
from pygame.locals import QUIT

last_frequency = 100.0
last_note = "A"
screen_height = 1300
screen_width = 2000
black = (0, 0, 0)
white = (255, 255, 255)
num_lines = 5
line_spacing = screen_height // (num_lines + 1)
line_thickness = 2

player = pygame.image.load("./assets/player.png")


def get_position():
    global last_note
    note_mapping = {
        "C2": 0,
        "C#2": 0,
        "D2": 1,
        "D#2": 1,
        "E2": 2,
        "F2": 3,
        "F#2": 3,
        "G2": 4,
        "G#2": 4,
        "A2": 5,
        "A#2": 5,
        "B2": 6,
        "B#2": 6,
        "C3": 7,
        "C#3": 7,
        "D3": 8,
        "D#3": 8,
        "E3": 9,
        "F3": 10,
        "F#3": 10,
        "G3": 11,
        "G#3": 11,
        "A3": 12,
        "A#3": 12,
    }
    try:
        position = note_mapping[last_note]
        print("note " + str(last_note) +  "position " + str(position))
        return position
    except:
        return 0


def read_audio():
    global last_frequency
    frequency.init()
    while True:
        current_frequency = frequency.get_frequency()
        if current_frequency > 0.0:
            last_frequency = current_frequency
        time.sleep(10 / 1000)


def draw_dot(position):
    line_number = position // 2
    if position % 2 == 0:  # Draw on the line
        y = (line_number + 1) * line_spacing
    else:  # Draw between lines
        y = (line_number + 1) * line_spacing - line_spacing // 2
    pygame.draw.circle(screen, black, (screen_width // 2, y), 100)


pygame.init()
screen = pygame.display.set_mode((screen_width, screen_height))
font = pygame.font.Font('freesansbold.ttf', 64)

running = True
pygame.display.set_caption('Sound Game!')

audio_thread = threading.Thread(target=read_audio)
audio_thread.start()


def draw_player():
    position = get_position()
    line_number = 7 - (position // 2)
    y = 0
    if position % 2 == 0:
        y = line_number * line_spacing
    else:  # Draw between lines
        y = line_number * line_spacing - (line_spacing // 2)
    # pygame.draw.circle(screen, black, (screen_width // 2, y), 100)
    screen.blit(player, (200, y))


def draw_text(text, x, y, colour):
    text_render = font.render(text, True, colour)
    text_rect = text_render.get_rect()
    text_rect.topleft = (x, y)
    screen.blit(text_render, text_rect)


def draw_note():
    global last_note, last_frequency
    last_note = frequency.get_note(last_frequency)
    draw_text(last_note, screen_width - 150, 10, black)


while running:
    screen.fill(white)
    for i in range(1, num_lines + 1):
        y = i * line_spacing
        pygame.draw.line(screen, black, (0, y), (screen_width, y), line_thickness)

    draw_dot(6)
    draw_note()

    if last_frequency is not None:
        draw_player()

    for event in pygame.event.get():
        if event.type == QUIT:
            audio_thread.join()
            pygame.quit()
            sys.exit()
    pygame.display.flip()
    pygame.display.update()
